<html>

<head>
    <?php 
    $pageTitle = 'Create Album - Gallery';
    $page = 'create';
    require_once 'partials/header.php'; ?>
</head>

<body>
    <?php require_once 'partials/navbar.php';

    // function for uploading the file
    function uploadImage($image)
    {
        $pathInfo = pathinfo($image['name']);
        $fileBaseName = $pathInfo['filename']; //meme
        $fileExt = $pathInfo['extension']; //jpg
        $uploadDir = "uploads";
        $index = 1;
        $fullName = $fileBaseName . "." . $fileExt; //meme.jpg
        while (file_exists($uploadDir . "/" . $fullName)) {
            $fullName = $fileBaseName . "-" . $index . "." . $fileExt; //meme-2.jpg
            $index++;
        }
        move_uploaded_file(
            $image['tmp_name'],
            $uploadDir . "/" . $fullName
        );

        return $fullName;
    }

    $values = ['title' => '', 'description' => '', 'cover' => ''];
    $formValues  = $values;

    if (!empty($_POST)) {

        //initialize at start that  we dont have errors yet
        $errors = [];

        $formValues = array_merge($values, $_POST);

        //first validate that we  have all the fields we need
        // validation for the input fields in the create album form
        if (empty($_POST['title']) || empty(trim($_POST['title']))) {
            $errors['title'] = "Album Title is required";
        }
        if (empty($_POST['description']) || empty(trim($_POST['description']))) {
            $errors['description'] = "Album Description is required";
        }
        // PHP validate for image
        if (empty($_FILES['cover']['name'])) {
            $errors['cover'] = 'Image cover is required';
        }
        if (empty($errors)) {
            //if no errors |^| -> first upload product image and get the name under what the image is saved
            
            
            $image = uploadImage($_FILES['cover']);
            $title = $formValues['title'];
            $description = $formValues['description'];
            $user_id = $_SESSION['id'];
            //then save the product in database
            $sql = "INSERT INTO `albums` (`title`, `description`, `cover`, `user_id`) 
                VALUES (
                    '" . $title . "', 
                    '" . $description . "',
                    '" . $image . "',
                    '" . $user_id . "'
                )";


            if ($stmt = $pdo->prepare($sql)) {
                // bind 
                $stmt->bindParam(":title", $param_title);
                $stmt->bindParam(":description", $param_description);
                $stmt->bindParam(":image", $param_image);

                // set
                $param_title = $title;
                $param_description = $description;
                $param_image = $image;


                if ($stmt->execute()) {
                    /* Kreirajte poraka za uspeshno vnesen ALBUM istata isprintajte ja vo index.php
                 vo box koj kje stoi 7 sekundi i potoa kje ischezna */

                    $SQLALBUM = "SELECT LAST_INSERT_ID() as id FROM albums";
                    if ($result = $pdo->prepare($SQLALBUM)) {
                        if ($result->execute()) {
                            if ($result->rowcount()) {
                                if ($row = $result->fetch()) {
                                    $id = $row["id"];
                                }
                            }
                        }
                        unset($result);
                    }
                    // die($pdo);
                    // redirect to upload with id parametar
                    header("location: upload.php?id=" . $id . "");
                } else {
                    echo "Something went wrong";
                }
                unset($stmt);
            } // end 2 if
        }
        unset($pdo);
        $formValues = $values; //Reset form values to be empty, otherwise the form will still have values from the product that we already have created
    }

    ?>

    <div class="main-content">
    <h3>Create Album</h3>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" novalidate name="create-album" enctype="multipart/form-data" id="create-form">

            <div class="form-group">
                <label for="inputTitle">Album Title</label>
                <input type="text" class="form-control" id="inputTitle" name="title" value="<?php if (isset($_POST['title'])) echo $_POST['title']; ?>">
                <span class="form-error">
                    <?php if (!empty($errors['title'])) {
                        echo $errors['title'];
                    } ?>
                </span>
                <!-- <div class="form-error" id="error-container">
                </div> -->
            </div>

            <div class="form-group">
                <label for="inputDesc">Album Description</label>
                <textarea class="form-control" id="inputDesc" rows="3" name="description"><?php if (isset($_POST['description'])) {
                                                                                                echo htmlentities($_POST['description']);
                                                                                            } ?></textarea>
                <span class="form-error">
                    <?php if (!empty($errors['description'])) {
                        echo $errors['description'];
                    } ?>
                </span>
                <!-- <div class="form-error" id="error-container">
                </div> -->
            </div>

            <div class="form-group">
                <label for="cover">Photo for Album Cover</label>

                <input type="file" class="form-control-file" placeholder="cover" accept="image/*" name="cover" id="inputCover">
                <span class="form-error">
                    <?php if (!empty($errors['cover'])) {
                        echo $errors['cover'];
                    } ?>
                </span>

            </div>

            <button type="submit" name="submit" class="btn btn-primary">Submit</button>

            <div class="form-error" id="error-container">
            </div>
        </form>
    </div>
    <?php require "partials/footer.php"; ?>
</body>

</html>