let formCreate = document.getElementById('create-form');

formCreate.addEventListener('submit', function(event) {

    // let errorDiv = document.getElementsByClassName('error-container');
    
    let errorDiv = document.getElementById('error-container');
    let title = document.querySelector('#inputTitle');
    let description = document.querySelector('#inputDesc');
    let cover = document.querySelector('#inputCover');
    
    errorDiv.innerHTML = '';
    
    if(!title.value) {
        addErrorMessage('title is required');
    }
    if(!description.value) {
        addErrorMessage("description is required");
    }

    if(!cover.value) {
        addErrorMessage("cover image is required");
    }

    if (errorDiv.innerHTML !== ''){
        event.preventDefault();
    } else {
        return true;
    }
    
    
});

function addErrorMessage(message) {
    event.preventDefault();
    let errorDiv = document.getElementById('error-container');
    let error = document.createElement('div');
    error.innerText =  message;
    errorDiv.append(error);
}

