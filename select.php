<?php
require_once 'partials/config.php';

if (isset($_GET["album_id"]) && !empty(trim($_GET["album_id"]))) {
     $output = '';

     // $sql = "SELECT COUNT(*), * FROM images WHERE albumId = :albumId";

     $sql = "SELECT *, count(*) as countImg FROM albums INNER JOIN images ON albums.a_id = images.albumId WHERE images.albumId = :albumId";

     if ($stmt = $pdo->prepare($sql)) {
          $stmt->bindParam(':albumId', $param_albumId);
          // $count = $stmt->fetchColumn();
     
          $param_albumId = trim($_GET['album_id']);

          if ($stmt->execute()) {
               if ($stmt->rowCount() > 0) {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    $title = $row['title'];
                    $description = $row['description'];
                    $timeStamp = $row['createdAt'];
                    $count = $row['countImg'];
                    
                    $output = '
          <div class="album-modal-body">
               <i class="fas fa-times" id="closeModal"></i>
               <div class="title">Album Name: ' . $title . '</div>
               <div class="description">Album Description: ' . $description . '</div>
               <div class="images"> Number of images: ' . $count . '</div>
               <div class="created">Created at: ' . $timeStamp . '</div>
           </div>
           ';
               }
          }
    
     }
     echo $output;
     unset($stmt);
     unset($pdo);
}
exit;
