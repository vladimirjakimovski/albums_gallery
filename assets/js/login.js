const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
	container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
	container.classList.remove("right-panel-active");
});


// register form validation

let registerForm = document.getElementById('sign-up-form');
registerForm.addEventListener('submit', function(event) {
    // let errorDiv = document.getElementsByClassName('error-container');
    let registerErrorDiv = document.getElementById('register-error-container');
    let usernameRegister = document.querySelector('#usernameRegister');
    let emailRegister = document.querySelector('#emailRegister');
    let passwordRegister = document.querySelector('#passwordRegister');
	registerErrorDiv.innerHTML = '';
	
    if(!usernameRegister.value) {
        addErrorMessageRegister('username is required');
    }
    if(!emailRegister.value) {
        addErrorMessageRegister("email is required");
    }
    if(!passwordRegister.value) {
        addErrorMessageRegister("password is required");
    }
	if (registerErrorDiv.innerHTML !== ''){
        event.preventDefault();
        return false;
    } else {
        return true;
    }
});

function addErrorMessageRegister(message) {
    let registerErrorDiv = document.getElementById('register-error-container');
    let registerError = document.createElement('div');
    registerError.innerText =  message;
    registerErrorDiv.append(registerError);
}


// login form validation

let loginForm = document.getElementById('log-in-form');
loginForm.addEventListener('submit', function(event) {
    // let errorDiv = document.getElementsByClassName('error-container');
    let loginErrorDiv = document.getElementById('login-error-container');
    let usernameLogin = document.querySelector('#usernameLogin');

    let passwordLogin = document.querySelector('#passwordLogin');
    loginErrorDiv.innerHTML = '';
    if(!usernameLogin.value) {
        addErrorMessageLogin('username is required');
    }
    if(!passwordLogin.value) {
        addErrorMessageLogin("password is required");
    }
	if (loginErrorDiv.innerHTML !== ''){
        event.preventDefault();
        return false;
    } else {
        return true;
    }
});

function addErrorMessageLogin(message) {
    let loginErrorDiv = document.getElementById('login-error-container');
    let errorLogin = document.createElement('div');
    errorLogin.innerText =  message;
    loginErrorDiv.append(errorLogin);
}


// username availability

$(document).ready(function(){

	$("#usernameRegister").keyup(function(){
 
	  var usernameRegister = $(this).val().trim();
 
	  if(usernameRegister != ''){
 
		 $.ajax({
			
			url: 'ajaxfile.php',
			type: 'post',
			data: {usernameRegister:usernameRegister},
			success: function(response){
			   // Show response
			   $("#uname_response").html(response);
 
			}
		 });
	  }else{
		 $("#uname_response").html("");
	  }
   });
// email availability

   $("#emailRegister").keyup(function(){
 
	var emailRegister = $(this).val().trim();

	if(emailRegister != ''){

	   $.ajax({
		  
		  url: 'ajaxfile.php',
		  type: 'post',
		  data: {emailRegister:emailRegister},
		  success: function(response){
			 // Show response
			 $("#emailRegister_response").html(response);

		  }
	   });
	}else{
	   $("#emailRegister_response").html("");
	}
 });
 
 });