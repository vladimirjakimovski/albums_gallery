

let uploadImage = document.getElementsByClassName('btn-secondary')[0];

uploadImage.addEventListener('click', function () {

    let fileInput = document.querySelector("input[type='file']");

    fileInput.click();
});


let fileInput = document.querySelector("input[type='file']");

fileInput.addEventListener('change', function (event) {
    var progress = document.querySelector('.progress');
    let files = event.target.files;
    let output = document.getElementById('result');
    // console.log(files);
    // fileReader.readAsDataURL(files[0]);
    // nf = fileInput.files.length;  // count numberOfFiles
    let previewDiv = document.getElementsByClassName('preview-images')[0];
    previewDiv.classList.add('visible');

    // fileReader.onload = function (ev) {
    // console.log(previewDiv);

    for (var i = 0; i < files.length; i++) {

        var file = files[i];
        if (!file.type.match('image'))
            continue;
        let fileReader = new FileReader();
        fileReader.onload = function (event) {
            var progress = document.querySelector('.progress');
            progress.style.width = '100%';
            let picFile = event.target;
            let Div = document.createElement('div');
            Div.innerHTML = "<img class='preview visible' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>  ";
            Div.classList.add('visible');
            output.insertBefore(Div, null);
        }

        fileReader.readAsDataURL(file);

        function updateprogress(event) {
            if(event.lengthComputable) {
                var loadedPercentage = Math.round((event.loaded/event.total)*100);
                progress.style.width = loadedPercentage + '%';
            }
        }
    
        fileReader.onprogress = updateprogress;

    }
    
    

});