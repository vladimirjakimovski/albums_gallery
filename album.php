<html>

<head>
    <?php
    $page = 'album';
    $pageTitle = 'Albums - Gallery';
    require_once 'partials/header.php'; ?>
</head>

<body>

    <?php require_once 'partials/navbar.php';

    ?>

    <div class="containerSlider">

        <div class="gallery-wrap">
            <?php
            if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {

                $paths = [];
                $dir = 'uploads'; // images directory

                $sql = "SELECT * FROM images INNER JOIN albums ON images.albumId = albums.a_id WHERE albumId = :id"; // working

                if ($stmt = $pdo->prepare($sql)) {
                    $stmt->bindparam(":id", $param_id);
                    $param_id = trim($_GET['id']); // get the id from url
                    if ($stmt->execute()) {
                        if ($stmt->rowCount() >= 0) {
                            $i = 1;
                            while ($row = $stmt->fetch()) {

            ?> <h1 id='title'>album:<?php echo $row['title']; ?> </h1>
                                <div onclick="openLightbox();toSlide(<?php echo $i; ?>)" class="item" style="background: url(uploads/<?= $row['name']; ?>) no-repeat;" ">
                                </div>
            <?php $i = $i + 1;
                            }
                        }
                    }
                    // unset($stmt);
                } else {
                    echo "We dont have record in the DB";
                    header("location: album.php");
                    exit();
                }
                // unset($pdo);
            }

            ?>

        </div>



    </div>

    <div id="Lightbox" class="modal">
                                    <div class="modal-content">
                                        <span class="close pointer" onclick="closeLightbox()">&times;</span>
                                        <?php
                                        require_once "partials/config.php";
                                        if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {

                                            $paths = [];
                                            $dir = 'uploads'; // images directory

                                            $sql = "SELECT * FROM images INNER JOIN albums ON images.albumId = albums.a_id WHERE albumId = :id"; // working

                                            if ($stmt = $pdo->prepare($sql)) {
                                                $stmt->bindparam(":id", $param_id);
                                                $param_id = trim($_GET['id']); // get the id from url
                                                if ($stmt->execute()) {
                                                    if ($stmt->rowCount() > 0) {
                                                        $i = 1;
                                                        while ($row = $stmt->fetch()) {
                                        ?>
                                                            <div class="slide">
                                                                <img src="uploads/<?= $row['name']; ?>" onclick="toSlide(<?= $i ?>)" class="image-slide" /></div>

                                        <?php
                                                            $i++;
                                                        }
                                                    }
                                                }
                                                unset($stmt);
                                            } else {
                                                echo "We dont have record in the DB";
                                                header("location: album.php");
                                                exit();
                                            }
                                            unset($pdo);
                                        }

                                        ?>
                                        <a class="previous" onclick="changeSlide(-1)">&#10094;</a>
                                        <a class="next" onclick="changeSlide(1)">&#10095;</a>
                                    </div>
                                </div>

        </div>
    </div>

    <?php require "partials/footer.php"; ?>
</body>

</html>