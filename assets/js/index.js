//  close modal for created album
var black = document.getElementById('blackout')
document.getElementById('closeButton').addEventListener('click', function (e) {
    e.preventDefault();
    this.parentNode.style.display = 'none';
    black.style.display = 'none';
}, false);

// function for 3d effect on albums
$(function () {
    var card = $(".card");
    card.on('mousemove', function (e) {
        var x = e.clientX - $(this).offset().left + $(window).scrollLeft();
        var y = e.clientY - $(this).offset().top + $(window).scrollTop();

        var rY = map(x, 0, $(this).width(), -17, 17);
        var rX = map(y, 0, $(this).height(), -17, 17);

        $(this).children(".image").css("transform", "rotateY(" + rY + "deg)" + " " + "rotateX(" + -rX + "deg)");
    });

    card.on('mouseenter', function () {
        $(this).children(".image").css({
            transition: "all " + 0.05 + "s" + " linear",
        });
    });

    card.on('mouseleave', function () {
        $(this).children(".image").css({
            transition: "all " + 0.2 + "s" + " linear",
        });

        $(this).children(".image").css("transform", "rotateY(" + 0 + "deg)" + " " + "rotateX(" + 0 + "deg)");
    });

    function map(x, in_min, in_max, out_min, out_max) {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
});

// finish function for 3d effect on albums

// loader

$(document).ready(function () {
    //  time for loader
    setTimeout(function () {
        $('body').addClass('loaded');
    }, 4500);


    
    // view info album
    $(document).on('click', '.view_data', function () {
        var album_id = $(this).attr("id");

        if (album_id != '') {
            $.ajax({
                url: "select.php",
                method: "GET",
                data: { album_id: album_id },
                success: function (data) {
                    // alert(data);
                    $('.album-modal-body').html(data);
                    $('.album-modal').addClass('visible');
                    $('#grayout').addClass('visible');

                    // close modal on X click
                    $('#closeModal').on('click', function () {
                        $('.album-modal').removeClass('visible');
                        $('#grayout').removeClass('visible');
                    });

                    $(document).on('click', function () {
                        $('.album-modal').removeClass('visible');
                        $('#grayout').removeClass('visible');
                    });

                
                }
            });
        }
    });





});