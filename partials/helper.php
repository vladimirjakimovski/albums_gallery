<?php

function uploadImage($file)
{
    $pathInfo = pathinfo($file['name']);
    $fileBaseName = $pathInfo['filename']; //meme
    $fileExt = $pathInfo['extension']; //jpg
    $uploadDir = "uploads";
    $index = 1;
    $fullName = $fileBaseName . "." . $fileExt; //meme.jpg
    while (file_exists($uploadDir . "/" . $fullName)) {
        $fullName = $fileBaseName . "-" . $index . "." . $fileExt; //meme-2.jpg
        $index++;
    }
    move_uploaded_file(
        $file['tmp_name'],
        $uploadDir . "/" . $fullName
    );

    return $fullName;
}

if (empty($errors)) {
    function saveProduct($title, $description, $image)
    {
        require_once "config.php";

        $sql = "INSERT INTO `albums` (`title`, `description`, `image`) 
                VALUES (
                    '" . $title . "', 
                    '" . $description . "',
                    '" . $image . "'
                )";


        if ($stmt = $pdo->prepare($sql)) {

            $stmt->bindParam(":title", $param_title);
            $stmt->bindParam(":description", $param_description);
            $stmt->bindParam(":image", $param_image);
            // $stmt->bindParam(":user_id", $param_user_id);

            $param_title = $title;
            $param_description = $description;
            $param_image = $image;
            // $param_user_id = $_SESSION['id'];
        }

        if ($stmt->execute()) {
            /* Kreirajte poraka za uspeshno vnesen record i istata isprintajte ja vo index.php
    vo box koj kje stoi 7 sekundi i potoa kje ischezna */
            header("location: index.php?added=1");
            exit();
        } else {
            echo "Something went wrong";
        }
        unset($stmt);
    } // end 2 if
    unset($pdo);
};
