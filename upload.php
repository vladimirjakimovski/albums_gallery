<html>

<head>
    <?php
    $pageTitle = 'Upload Images to Album - Gallery';
    $page = 'upload';
    require_once 'partials/header.php'; ?>
</head>

<body>
    <?php require_once 'partials/navbar.php';

    $values = ['albumId' => '', 'imagesName' => ''];
    $formValues  = $values;

    if (!empty($_POST)) {
        //initialize at start that  we dont have errors yet
        $errors = [];

        // Count total files
        $countfiles = count($_FILES['images']['name']);
        // merge the values from the form into the values
        $formValues = array_merge($values, $_POST);

        //first validate that we  have all the fields we need
        // validation for the input fields in the create album form
        if (empty($_POST['albumId']) || empty(trim($_POST['albumId']))) {
            $errors['albumId'] = "Album ID is required";
        }
        // PHP validate for image
        if (empty($_FILES['images']['name'])) {
            $errors['imagesName'] = 'Image cover is required';
        }

        // if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
        //     //ovde go zemame URL parametarot
        //     $id = trim($_GET["id"]);

        //     // SQL stmt
        //     $sql = "SELECT * FROM albums WHERE id = :id";

        //     //Prepare
        //     if ($stmt = $pdo->prepare($sql)) {
        //         $stmt->bindParam(":id", $param_id);
        //         //Set
        //         $param_id = $id;

        //         //Execute
        //         if ($stmt->execute()) {
        //             if ($stmt->rowCount() == 1) {
        //                 $row = $stmt->fetch(PDO::FETCH_ASSOC);
        //             } else {
        //                 // nema validen id paramatar i ne nosi na erro page-ot
        //                 header("location: error.php");
        //                 exit();
        //             }
        //         } else {
        //             echo "SMth went wrong";
        //         }
        //     }
        // }


        //if no errors |^| -> first upload product image and get the name under what the image is saved
        if (empty($errors)) {

            // album id from the form
            $albumId = $formValues['albumId'];
            // directory for uploaded files
            $uploadDir = "uploads";

            // loop all files
            for ($i = 0; $i < $countfiles; $i++) {

                // one file image name from the form
                $pathInfo = pathinfo($_FILES['images']['name'][$i]);
                $fileBaseName = $pathInfo['filename']; //name
                $fileExt = $pathInfo['extension']; //jpg

                $fullName = basename($_FILES['images']['name'][$i]); // full img name
                $index = 1;

                // $fullName = $fileBaseName . "." . $fileExt; //name.jpg
                while (file_exists($uploadDir . "/" . $fullName)) {
                    $fullName = $fileBaseName . "-" . $index . "." . $fileExt; //name-2.jpg
                    $index++;
                }

                // valid file image extensions
                $validExtension = array("png", "jpeg", "jpg");

                // if valid upload file
                if (in_array($fileExt, $validExtension)) {

                    move_uploaded_file($_FILES['images']['tmp_name'][$i], $uploadDir . "/" . $fullName);

                    //then save the product in database
                    $sql = "INSERT INTO images (name, albumId, user_id) VALUES (:name, :albumId, :user_id)";
                    if ($stmt = $pdo->prepare($sql)) {

                        // bind  
                        $stmt->bindParam(":name", $param_imagesName);
                        $stmt->bindParam(":albumId", $param_albumId);
                        $stmt->bindParam(":user_id", $param_user_id);

                        // set
                        $param_imagesName = $fullName;
                        $param_albumId = $albumId;
                        $param_user_id = $_SESSION['id'];

                        // var_dump($_SESSION['id']);die;
                        // execute
                        if ($stmt->execute()) {
                            // header("location: upload.php?id=" . $id . "");
                            // redirect to upload with id parametar za uspeshno vnesen album so slikiv
                            $_SESSION['added'] = true;
                            // header("location: index.php?added=1&id=" . $id . "");
                            header("location: index.php");
                        } else {
                            echo "Something went wrong";
                        }
                        unset($stmt);
                    } // end 2 if
                } else {
                    $errors['imagesName'] = "Choose valid file format" . '<br/>' . "Valid formats: jpg, jpeg, png.";
                }
            }


            $formValues = $values;
            //Reset form values to be empty, otherwise the form will still have values from the product that we already have created
        } else {
            header("location: error.php");
            exit();
        }
        unset($stmt);
        unset($pdo);
    }


    ?>

    <div class="main-content">
        <h3>Album For The Uploaded Images&nbsp;<i class="fas fa-arrow-down"></i></h3>
        <form method="post" enctype="multipart/form-data" id="album-images-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" novalidate name="album-images-form">
            <div class="form-group">
                <div class="input-group">

                    <select required class="custom-select" id="select-album" name="albumId">

                        <option value="" hidden disabled selected>Select Album</option>
                        <?php
                        $id = trim($_SESSION['id']);

                        $sql = "SELECT * FROM albums INNER JOIN users ON albums.user_id = users.id WHERE albums.user_id = :id ORDER BY albums.a_id DESC";
                        // $sql = "SELECT * FROM albums";

                        $result = $pdo->prepare($sql);

                        $result->bindParam(':id', $param_id);

                        $param_id = $id;

                        if ($result->execute()) {
                            if ($result->rowCount() > 0) {
                                while ($row = $result->fetch()) { ?>
                                    <option value="<?= $row['a_id']; ?>" <?php if (isset($_GET['id'])) {
                                                                                if ($row['a_id'] == $_GET['id']) echo 'selected';
                                                                            } ?>> <?= $row['title']; ?></option>;
                                <?php } ?>
                                <div class="form-error">
                                    <?php if (!empty($errors['albumId'])) {
                                        echo $errors['albumId'];
                                    } ?>
                                </div>
                    </select>
            <?php
                            }
                            unset($result);
                        } else {
                            echo "We dont have record in the DB";
                        }
                        unset($pdo);
            ?>
                </div>
            </div>
            <input type="file" id="upload-hidden" accept="image/*" multiple style="visibility: hidden" name="images[]">
            <div class="form-error">
                <?php if (!empty($errors['imagesName'])) {
                    echo $errors['imagesName'];
                } ?>
            </div>
            <div class="preview-images">
                <p>PROGRESS% &nbsp;</p>
                <output id="result" />
                <div>
                    <div class='progress'></div>
                </div>

            </div>
            <div class="form-group">
                <button type="button" class="btn  btn-secondary btn-sm" id="upload-btn">
                    <i class="fas fa-camera-retro"></i> Upload Images
                </button>
            </div>
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <button type="submit" class="btn btn-primary">Submit Images</button>
        </form>
    </div>
    <?php require "partials/footer.php"; ?>
</body>


</html>