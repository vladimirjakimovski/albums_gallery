<nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
    <a class="navbar-brand" href="index.php">
        <img src="assets/images/logo-albums.png" class="d-inline-block align-top" alt="" loading="lazy">
        Albums Gallery
    </a>
    <?php if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) : ?>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a id="hover-links1" <?= ($page == 'create') ? "class='activeA'" : ""; ?> class="nav-item nav-link" href="create.php">Create <span class="sr-only">(current)</span></a>
            <a id="hover-links2" <?= ($page == 'upload') ? "class='activeA'" : ""; ?> class="nav-item nav-link" href="upload.php">Upload</a>
        </div>
        <div class="ml-auto p-2 bd-highlight navbar-nav">
        <?php if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) : ?>
            <a class="login" href="login.php">Login</a>
        <?php endif; ?>   
            <a href="logout.php" class="logout">Logout <?php echo $_SESSION['username']; ?> &nbsp;<i class="fas fa-sign-out-alt"></i></a>
        </div>

        
    </div>
    <?php endif; ?>
</nav>