<?php

require_once "partials/config.php";

// username check availability
if(isset($_POST['usernameRegister'])){
   $username = $_POST['usernameRegister'];

   // Check username
   $sql = 'SELECT count(*) as cntUser FROM users WHERE username=:username';
   $stmt = $pdo->prepare($sql);
   $stmt->bindValue(':username', $username, PDO::PARAM_STR);
   $stmt->execute(); 
   $count = $stmt->fetchColumn();

   $response = "<span style='color: green;'>Username Available.</span>";
   if($count > 0){
      $response = "<span style='color: red;'>Username Not Available.</span>";
   }

   echo $response;
   exit;
}

// email check availability
if(isset($_POST['emailRegister'])){
    $email = $_POST['emailRegister'];
 
    // Check username
    $sql = 'SELECT count(*) as cntUser FROM users WHERE email=:email';
    $stmt = $pdo->prepare($sql);
    $stmt->bindValue(':email', $email, PDO::PARAM_STR);
    $stmt->execute(); 
    $count = $stmt->fetchColumn();
 
    $response = "<span style='color: green;'>Email Available.</span>";
    if($count > 0){
       $response = "<span style='color: red;'>Email Not Available.</span>";
    }
 
    echo $response;
    exit;
 }

?>