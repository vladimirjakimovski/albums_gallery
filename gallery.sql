-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2020 at 05:19 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gallery`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `a_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`a_id`, `title`, `description`, `cover`, `createdAt`, `user_id`) VALUES
(3, 'Portraits', 'Images from people', 'fashion-man-person-holiday-36469.jpg', '2020-06-21 22:58:29', 5),
(77, 'Food', 'Images from delicious food', 'cooked-food-704569.jpg', '2020-07-06 17:14:21', 5),
(78, 'Architecture', 'Building photos and beautiful architecture', 'buildings-clear-sky-exterior-facade-374023.jpg', '2020-07-06 17:17:14', 5),
(79, 'Ocean Water', 'Photos from the ocean', 'high-angle-photography-of-ocean-991012.jpg', '2020-07-06 17:43:55', 6),
(80, 'Nature', 'Nature photos', 'air-bubbles-art-background-bright-531643.jpg', '2020-07-06 17:48:50', 5);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `albumId` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `albumId`, `user_id`) VALUES
(152, 'adult-black-pug-1851164.jpg', 3, 5),
(153, 'business-businessman-contemporary-corporate-532220.jpg', 3, 5),
(154, 'close-up-portrait-of-a-antelope-257558.jpg', 3, 5),
(155, 'fashion-man-person-holiday-36469.jpg', 3, 5),
(156, 'woman-sitting-and-smiling-1858175.jpg', 3, 5),
(157, 'woman-smiling-1102341.jpg', 3, 5),
(158, 'appetizer-bowl-chili-close-up-286283.jpg', 77, 5),
(159, 'chocolates-and-raspberries-918327.jpg', 77, 5),
(160, 'cooked-food-704569-1.jpg', 77, 5),
(161, 'pepperoni-pizza-with-basil-leaves-1260968.jpg', 77, 5),
(162, 'person-holding-broccoli-3727689.jpg', 77, 5),
(163, 'person-holding-pineapple-fruit-2940256.jpg', 77, 5),
(164, 'vegetable-salad-3026808.jpg', 77, 5),
(165, 'aerial-architectural-design-architecture-buildings-373912.jpg', 78, 5),
(166, 'architecture-blue-sky-buildings-business-290275.jpg', 78, 5),
(167, 'architecture-buildings-business-city-358502.jpg', 78, 5),
(168, 'blue-and-gray-high-rise-building-162031.jpg', 78, 5),
(169, 'buildings-clear-sky-exterior-facade-374023-1.jpg', 78, 5),
(170, 'mirror-facade-of-tall-building-1963557.jpg', 78, 5),
(171, 'photography-of-roadway-during-dusk-1034662.jpg', 78, 5),
(172, 'a-glass-of-water-and-a-red-sun-1916750.jpg', 79, 6),
(173, 'architecture-buildings-business-city-358502-1.jpg', 79, 6),
(174, 'back-light-beads-blur-bubble-270410.jpg', 79, 6),
(175, 'blue-and-gray-high-rise-building-162031-1.jpg', 79, 6),
(176, 'clear-water-with-water-drop-3520799.jpg', 79, 6),
(177, 'closeup-photo-of-water-drop-932320.jpg', 79, 6),
(178, 'high-angle-photography-of-ocean-991012-1.jpg', 79, 6),
(179, 'shallow-focus-photo-of-glass-ball-near-body-of-water-2033994.jpg', 79, 6),
(180, 'water-bubbles-3445716.jpg', 79, 6),
(181, 'closeup-photo-of-short-coated-white-and-gray-dog-825947.jpg', 78, 5),
(182, 'air-bubbles-art-background-bright-531643-1.jpg', 80, 5),
(183, 'appetizer-bowl-chili-close-up-286283-1.jpg', 80, 5),
(184, 'closeup-photo-of-water-drop-932320-1.jpg', 80, 5),
(185, 'vegetable-salad-3026808-1.jpg', 80, 5),
(186, 'water-bubbles-3445716-1.jpg', 80, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `created_at`) VALUES
(1, 'vlatko', 'vladimirjakimovski@gmail.com', '$2y$10$YFEvIC1XEwXWNZPYjBTDJOeiGIwnwZaYR/YnEeWbJV36mv6JXxrpO', '2020-07-01 04:12:20'),
(2, 'proba', 'proba@gmail.com', '$2y$10$/oXV/2aOtJsMPxUZVk7JSeTEx9UyQS/cnyEPZUWqML3fbQhtR3J0u', '2020-07-04 18:26:44'),
(5, 'vladimir', 'vladimir@gmail.com', '$2y$10$Ra1G/3wAj8ZRJ6aA88wSi.MNDnpr.dHy2x4VPjwE2G4ENgj8dXSVm', '2020-07-05 03:34:17'),
(6, 'jakimovski', 'jakimovski@gmail.com', '$2y$10$SiEKpPYND.fHXHuLuYZxZOvGUcA0P.8mBIFGdWDuHjsWUA/T7lYBC', '2020-07-06 17:41:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`a_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_ibfk_1` (`albumId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`albumId`) REFERENCES `albums` (`a_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
