<?php
define('DB_SERVER','localhost');
define('DB_USERNAME','root');
define('DB_PASSWORD','');
define('DB_NAME','gallery');
//probuvame da se konektirame so bazata
try {
    $pdo = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USERNAME, DB_PASSWORD);
    // za error exeption
    $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    // echo "Konekcijata so Bazata preku PDO e uspeshna";
    // poraka za uspeshna konekcija
    } catch(PDOException $e) {
        echo "Konekcijata ne e uspeshna". $e->getMessage();
    }
?>