<html>

<head>
    <?php 
    $pageTitle = 'Error 404 Page - Gallery';
    $page= 'error';
    require_once 'partials/header.php'; ?>
</head>

<body>
    <?php require_once 'partials/navbar.php'; ?>


    <div class="error-container">
        <div class="header-label">
            <h3>Error 404</h3>
        </div>
        <hr>
        <div class="error-wrapper">
             <p>Sorry this is a invalid request</p>
                <div class="button-wrapper">
                    <a href="index.php" class="buttons back">Back</a>
                </div>
        </div>

    </div>

    <?php require "partials/footer.php"; ?>
</body>

</html>