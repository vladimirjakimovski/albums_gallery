<html>

<head>
	<?php

	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
		header('location: index.php');
		exit;
	}

	$title = "Register/Login - Gallery";
	$page = 'login';
	$pageTitle = 'Login & Registration - Gallery';
	require_once 'partials/header.php';

	//definiranje na promenlivite i nivna inicijalizacija so prazni vrednosti
	$usernameRegister = $usernameLogin = $emailRegister = $email = $passwordRegister = $passwordLogin = "";
	$usernameRegister_err = $usernameLogin_err = $emailRegister_err = $email_err = $passwordRegister_err = $passwordLogin_err = "";


	//procesiranje na podatocite za register
	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		// register related check
		if (isset($_POST['submitRegister'])) {
			//validacija na username
			if (empty(trim($_POST["usernameRegister"]))) {
				$usernameRegister_err = "Please enter username";
			} else {
				//prepare SELECT
				$sql = "SELECT id FROM users WHERE username = :username";

				if ($stmt = $pdo->prepare($sql)) {
					$stmt->bindParam(":username", $param_username);

					//setiranje na parametri
					$param_username = trim($_POST["usernameRegister"]);
				}
				//da izvrsime prepared statement
				if ($stmt->execute()) {
					if ($stmt->rowCount() == 1) {
						$usernameRegister_err = "This username is taken.";
					} else {
						$usernameRegister = trim($_POST["usernameRegister"]);
					}
				} else {
					echo "Smth is wrong";
				}
				unset($stmt);
			}

			//Validacija na email
			if (empty(trim($_POST["emailRegister"]))) {
				$emailRegister_err = "Please enter email";
			} elseif (!filter_var($_POST["emailRegister"], FILTER_VALIDATE_EMAIL)) {
				$emailRegister_err = "Invalid email format";
			} else {
				$emailRegister = trim($_POST["emailRegister"]);
			}

			//validacija na password
			if (empty(trim($_POST["passwordRegister"]))) {
				$passwordRegister_err = "Please enter a password.";
			} elseif (strlen(trim($_POST["passwordRegister"])) < 6) {
				$passwordRegister_err = "Password must have atleast 6 characters.";
			} else {
				$passwordRegister = trim($_POST["passwordRegister"]);
			}


			// proverka na input errors
			if (empty($usernameRegister_err) && empty($emailRegister_err) && empty($passwordRegister_err)) {
				//podgotovka za insert stat
				$sql = "INSERT INTO users (username, password, email) VALUES (:username, :password, :email)";

				if ($stmt = $pdo->prepare($sql)) {

					//bind na varijabli

					$stmt->bindParam("username", $param_username);
					$stmt->bindParam("email", $param_email);
					$stmt->bindParam("password", $param_password);


					//setiranje na parametri 
					$param_username = $usernameRegister;
					$param_email = $emailRegister;
					$param_password = password_hash($passwordRegister, PASSWORD_DEFAULT);

					//izvrsuvanje na stmt

					if ($stmt->execute()) {
						//Redirect kon login page
						header("location: login.php");
					} else {
						echo "Smth went wrong.";
					}
					unset($stmt);
				}
			}
			unset($pdo);
		} else {
			// LOGIN related check

			// dali username e prazen
			if (empty(trim($_POST["usernameLogin"]))) {
				$usernameLogin_err = "Please enter your username.";
			} else {
				$usernameLogin = trim($_POST["usernameLogin"]);
			}
			// dali password e prazen
			if (empty(trim($_POST["passwordLogin"]))) {
				$passwordLogin_err = "Please enter your password.";
			} else {
				$passwordLogin = trim($_POST["passwordLogin"]);
			}

			if (empty($emailLogin_err) && empty($passwordLogin_err)) {
				$sql = "SELECT id, username,password FROM users WHERE username = :username";
				if ($stmt = $pdo->prepare($sql)) {
					$stmt->bindParam(":username", $param_username);

					$param_username = trim($_POST["usernameLogin"]);

					if ($stmt->execute()) {
						// da proverime dali usernamot postoi

						if ($stmt->rowCount() == 1) {
							if ($row = $stmt->fetch()) {
								$id = $row['id'];
								$usernameLogin = $row['username'];
								$hashed_password = $row['password'];
								if (password_verify($passwordLogin, $hashed_password)) {

									$_SESSION['loggedin'] = true;
									$_SESSION['id'] = $id;
									$_SESSION['username'] = $usernameLogin;
									// $_SESSION['views']= 0; 
									header("location:index.php");
								} else {
									// greshka za password

									$passwordLogin_err = "The password you entered is not correct";
								}
							}
						} else {
							$usernameLogin_err = "No user with that username";
						}
					} else  echo "smtg is wrong";
				}
				unset($stmt);
			}
			unset($pdo);

			// end if POST
		}
	}

	?>
	<link rel="stylesheet" href="assets/styles/login.css">
</head>

<body>
	<?php require_once 'partials/navbar.php'; ?>

	<div class="container" id="container">
		<div class="form-container sign-up-container">
			<form id='sign-up-form' action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
				<h1>Create Account</h1>

				<span>Use your email for registration</span>
				<div id="uname_response"></div>
				<div class="<?php echo (!empty($usernameRegister_err)) ? 'has-error' : ''; ?>">
					<input name="usernameRegister" id="usernameRegister" type="text" placeholder="Username" value="<?php echo $usernameRegister; ?>" />
				</div>

				<div id="emailRegister_response"></div>
				<div class="<?php echo (!empty($emailRegister_err)) ? 'has-error' : ''; ?>">
					<input name="emailRegister" id="emailRegister" type="email" placeholder="Email" value="<?php echo $emailRegister; ?>" />
				</div>
				<div id="uname_response"></div>
				<div class="<?php echo (!empty($passwordRegister_err)) ? 'has-error' : ''; ?>">
					<input name="passwordRegister" id="passwordRegister" type="password" placeholder="Password" value="<?php echo $passwordRegister; ?>" />
				</div>

				<input type="submit" name="submitRegister" value="Sign Up" />

				<div class="form-error" id="register-error-container"></div>

				<span class="help-block"><?php echo $usernameRegister_err; ?></span>
				<span class="help-block"><?php echo $emailRegister_err;    ?></span>
				<span class="help-block"><?php echo $passwordRegister_err; ?></span>

			</form>
		</div>
		<div class="form-container sign-in-container">
			<form id="log-in-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
				<h1>Sign in</h1>

				<span>Use your account</span>

				<div class="<?php echo (!empty($usernameLogin_err)) ? 'has-error' : ''; ?>">
					<input id="usernameLogin" name="usernameLogin" type="text" placeholder="Username" value="<?php echo $usernameLogin; ?>" />
				</div>
				<div id="uname_response"></div>
				<div class="<?php echo (!empty($passwordLogin_err)) ? 'has-error' : ''; ?>">
					<input id="passwordLogin" name="passwordLogin" type="password" placeholder="Password" value="<?php echo $passwordLogin; ?>" />
				</div>

				<a href="#">Forgot your password?</a>

				<input type="submit" name="submitLogin" value="Sign In" />

				<div class="form-error" id="login-error-container"></div>

				<span class="help-block"><?php echo $usernameLogin_err; ?></span>
				<span class="help-block"><?php echo $passwordLogin_err; ?></span>
			</form>
		</div>
		<div class="overlay-container">
			<div class="overlay">

				<div class="overlay-panel overlay-left">
					<h1>Welcome Back!</h1>
					<p>To keep connected with us please login with your personal info</p>
					<button class="ghost" id="signIn">Sign In</button>
				</div>

				<div class="overlay-panel overlay-right">
					<h1>Hello, Friend!</h1>
					<p>Enter your personal details and start journey with us</p>
					<button class="ghost" id="signUp">Sign Up</button>
				</div>

			</div>
		</div>
	</div>


	<?php require "partials/footer.php"; ?>

</body>

</html>