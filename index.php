<html>

<head>
    <?php
    $page = 'index';
    $pageTitle = "Homepage - Gallery";
    require_once 'partials/header.php';

    // redirect user to login page if its not loggedin
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }

    // page views count
    // for displaying the loader only on the first visit on the index page
    if (!isset($_SESSION['views']))
        $_SESSION['views'] = 0;

    $_SESSION['views'] = $_SESSION['views'] + 1;

    ?>
</head>

<body>
    <?php if ($_SESSION['views'] == 1) : ?>
        <div id="loader-wrapper">
            <div id="loader"></div>
            <h1 id="welcome-username">Welcome <b><?php echo $_SESSION['username']; ?></b> !</h1>
            <p id="welcome-username-p">Printing out the albums for you...</p>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
    <?php endif; ?>

    <?php require_once 'partials/navbar.php';

    // function for shortening the description
    function truncate($text, $chars = 120)
    {
        if (strlen($text) > $chars) {
            $text = $text . ' ';
            $text = substr($text, 0, $chars);
            $text = substr($text, 0, strrpos($text, ' '));
            $text = $text . '...';
        }
        return $text;
    }

    ?>
    <!-- modal for added record to database -->
    <div id="blackout"></div>
    <div id="modal">
        <button id="closeButton">close</button>
        <div class="message">
            <h5>Alert</h5>
            <i class="fas fa-info-circle fa-4x"></i>
            <h3>The record has been succesfully inserted into the database!</h3>
        </div>
    </div>
    <!-- modal for the album info -->
    <div id="grayout"></div>
    <div class="album-modal">
        <div class="album-modal-body">
            <i class="fas fa-times" id="closeModal"></i>
            <div class="title">Some title</div>
            <div class="images">Images 12</div>
            <div class="created">22.01.2020</div>
        </div>
    </div>

    <div id="mainWrapper">
        <div id="cardsWrapper">
            <?php
            $id = trim($_SESSION['id']);

            $sql = "SELECT * FROM albums INNER JOIN users ON albums.user_id = users.id WHERE albums.user_id = :id ORDER BY albums.a_id DESC";

            $result = $pdo->prepare($sql);

            $result->bindParam(':id', $param_id);

            $param_id = $id;

            if ($result->execute()) {
                if ($result->rowCount() > 0) {
                    while ($row = $result->fetch()) { ?>

                        <div class="card">
                            <div class="image" style="background-image: url(uploads/<?= $row['cover']; ?>); background-size:cover; background-position: center;">
                                <div class="screen"></div>
                                <div class="text">
                                    <p id="title"><a href="album.php?id=<?= $row['a_id']; ?>"><?= $row['title'] ?></a></p>
                                    <p id="description"><a href="album.php?id=<?= $row['a_id']; ?>"><?= truncate($row['description'], 55); ?></a></p>
                                </div>


                            </div>
                            <div class="details">
                                <i class="fas fa-camera-retro"></i>
                                <i id="<?= $row["a_id"];  ?>" class="fas fa-info-circle album-info view_data"></i>
                                <input type="hidden" name="album_id" value="<?php echo $row["a_id"]; ?>" />

                            </div>
                        </div>
                    <?php } ?>

            <?php
                } else {
                    header("location:create.php");
                }
                unset($result);
            } else {
                echo "We dont have record in the DB";
            }

            unset($pdo);
            ?>

        </div>

    </div>

    <?php require "partials/footer.php";

    $recordAdded = false;

    if (isset($_SESSION['added']) && ($_SESSION['added'] == true)) {
        $recordAdded = true;
    }

    if ($recordAdded) {
        echo '
    <script type="text/javascript">
    function hideMsg()
    {
        document.getElementById("modal").style.display = "none";
        document.getElementById("blackout").style.display = "none";
    }
    document.getElementById("modal").style.display = "block";
    document.getElementById("blackout").style.display = "block";
    
    window.setTimeout("hideMsg()", 3000);
    
    </script>';
    }
    unset($_SESSION['added']);
    $recordAdded = false;
    ?>
</body>

</html>