<div class="footer-container">
    <footer>
        <p>Copyright &copy 2020 - Vladimir Jakimovski</p>
    </footer>
</div>

<script defer src="assets/js/album.js"></script>
<script defer src="assets/js/create.js"></script>
<script defer src="assets/js/index.js"></script>
<script defer src="assets/js/upload.js"></script>
<script src="https://kit.fontawesome.com/9bff7dd13f.js" crossorigin="anonymous"></script>
<script defer src="assets/js/login.js"></script>

